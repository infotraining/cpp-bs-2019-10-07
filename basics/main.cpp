#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    char c = 65;
    char character = 'A';

    cout << static_cast<int>(c) << " " << character << endl;

    // c-style cast
    cout << (int)c << " " << character << endl;

    const char* txt = "text";

    //    bad casting
    //    char* buggy = const_cast<char*>(txt);
    //    buggy[0] = 'T';

    int x = 10;

    const int* ptr_x = &x;

    int* mut_ptr_x = const_cast<int*>(ptr_x);
    *mut_ptr_x = 42;

    cout << "x = " << x << endl;
}
