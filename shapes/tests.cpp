#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include "shape.hpp"

using namespace std;
using namespace Drawing;

TEST_CASE("shape")
{
    Circle c(10, 20, 30);
    c.draw();
    c.move(100, 200);
    c.draw();

    Shape* ptr_shape = &c;
    ptr_shape->draw();

    Rectangle rect(2, 4, 100, 200);
    ptr_shape = &rect;
    ptr_shape->draw();

    cout << "\n";

    vector<unique_ptr<IShape>> shapes;
    shapes.push_back(make_unique<Circle>(1, 1, 10));
    shapes.push_back(make_unique<Rectangle>(10, 30, 100, 20));
    shapes.push_back(make_unique<Circle>(c)); // copy to heap
    shapes.push_back(make_unique<Rectangle>(std::move(rect))); // move to heap

    for (const auto& s : shapes)
    {
        s->draw();
    }
}
