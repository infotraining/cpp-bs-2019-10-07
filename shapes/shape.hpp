#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>

namespace Drawing
{
    struct Point
    {
        int x, y;

        void translate_to(int new_x, int new_y)
        {
            x += new_x;
            y += new_y;
        }
    };

    std::ostream& operator<<(std::ostream& out, const Point& pt);

    class IShape
    {
    public:
        virtual void move(int dx, int dy) = 0;
        virtual void draw() const = 0;
        virtual ~IShape(){};
    };

    class Shape : public IShape
    {
    protected:
        Point coord_;

    public:
        Shape(int x = 0, int y = 0)
            : coord_ {x, y}
        {
        }

        Shape(Point pt)
            : coord_ {pt}
        {
        }

        void move(int dx, int dy) override
        {
            coord_.translate_to(dx, dy);
        }

        virtual ~Shape() {}
    };

    class Circle : public Shape
    {
        int radius_;

    public:
        Circle(int x = 0, int y = 0, int r = 0)
            : Shape(x, y)
            , radius_(r)
        {
        }

        void draw() const override
        {
            std::cout << "Drawing circle at " << coord_ << " with r = " << radius_ << "\n";
        }
    };

    class Rectangle : public Shape
    {
        int w_;
        int h_;

    public:
        Rectangle(int x = 0, int y = 0, int w = 0, int h = 0)
            : Shape(x, y)
            , w_(w)
            , h_(h)
        {
        }

        void draw() const override
        {
            std::cout << "Drawing rectangle at " << coord_
                      << " with w = " << w_ << " & h = " << h_ << "\n";
        }
    };
}

#endif // SHAPE_HPP
