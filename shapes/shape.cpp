#include "shape.hpp"

std::ostream& Drawing::operator<<(std::ostream& out, const Drawing::Point& pt)
{
    out << "[" << pt.x << ", " << pt.y << "]";

    return out;
}
