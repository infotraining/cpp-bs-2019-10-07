#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("native array")
{
    SECTION("declaration - zeroed")
    {
        int tab[3] = {};

        for (int item : tab)
        {
            cout << item << " ";
        }
        cout << "\n";
    }

    SECTION("declaration - initialization")
    {
        int tab1[3] = { 1, 2, 3 };

        int tab2[] = { 1, 2, 3, 4 }; // size of array is deduced by compiler

        const int tab3[10] = { 1, 2, 3 };

        for (int item : tab3)
        {
            cout << item << " ";
        }
        cout << "\n";

        //tab3[2] = 10;  c-error

        REQUIRE(tab3[1] == 2);
    }
}

TEST_CASE("arrays & pointers")
{
    int tab[] = { 1, 2, 3, 4, 5, 6, 7 };

    int* ptr = tab;
    int* alt_ptr = &tab[0];

    REQUIRE(*ptr == 1);
    REQUIRE(*ptr == *alt_ptr);
    REQUIRE(ptr == alt_ptr);

    for (int* it = tab; it != tab + (sizeof(tab) / sizeof(int)); ++it)
    {
        cout << *it << " ";
    }
    cout << "\n";

    SECTION("since C++11")
    {
        for (int* it = begin(tab); it != end(tab); ++it)
        {
            cout << *it << " ";
        }
        cout << "\n";

        SECTION("range based for")
        {
            for (int x : tab)
            {
                cout << x << " ";
            }
            cout << "\n";

            for (char c : "text")
            {
                cout << c << " ";
            }
            cout << "\n";

            for (int x : { 1, 2, 3 })
            {
                cout << x << " ";
            }
            cout << "\n";
        }
    }
}

TEST_CASE("range based for")
{
    int tab[] = { 1, 2, 3 };

    for (int& x : tab)
    {
        ++x;
    }

    SECTION("is interpreted as")
    {
        for (int* it = begin(tab); it != end(tab); ++it)
        {
            int& x = *it;
            ++x;
        }
    }

    for (const int& x : tab)
    {
        cout << x << " ";
    }
    cout << "\n";

    string words[] = { "Hello", "World", "C++", "!" };

    for (const string& w : words)
    {
        cout << w << "\n";
    }
}
