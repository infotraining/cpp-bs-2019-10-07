#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Data
{
    char c;
    int a;
    double b;
    string name;
    int data[10];
};

void print(const Data& data)
{
    cout << "Data( " << data.c << ", " << data.a << ", " << data.b << ", \"" << data.name << "\", [ ";
    for (const auto& item : data.data)
        cout << item << " ";
    cout << "])\n";
}

TEST_CASE("struct")
{
    cout << "sizeof(char): " << sizeof(char) << endl;
    cout << "sizeof(int): " << sizeof(int) << endl;
    cout << "sizeof(double): " << sizeof(double) << endl;
    cout << "sizeof(string): " << sizeof(string) << endl;
    cout << "sizeof(int[10]): " << sizeof(int[10]) << endl;

    cout << "sizeof(Data) = " << sizeof(Data) << endl;

    Data data1 = {};
    print(data1);

    Data data2 { 'A', 45, 3.14, "text", { 1, 2, 3 } };
    print(data2);

    Data data3 { 'B' };
    print(data3);

    Data* ptr_data = &data3;

    (*ptr_data).name = "data3";
    ptr_data->c = 'D';

    print(*ptr_data);

    cout << "\n";

    vector<Data> vec = { { 'a', 42 }, { 'b', 665, 3.14, "pi" }, { 'c', 667, 34.44, "shady", { 1, 2, 3 } } };
    vec.push_back(Data { 's', 666, 44.44, "evil", { 666, 666, 666 } });
    vec.push_back(*ptr_data);

    for (const auto& item : vec)
        print(item);
}

struct Person
{
    string first_name;
    string last_name;
};

void print(const Person& p)
{
    cout << p.first_name << " " << p.last_name << "\n";
}

TEST_CASE("using Person")
{
    Person p1 { "Jan", "Kowalski" };
    print(p1);

    string last_name = "Nowak";
    Person p2 { "Adam", last_name };
    print(p2);

    vector<Person> people;
    people.push_back(p1);

    {
        string first_name = "Ewa______________________________________";
        Person p3 { first_name, last_name };
        people.push_back(p3);
    }

    int tab[256];
    fill(begin(tab), end(tab), 666);

    for (const auto& p : people)
    {
        print(p);
    }

    cout << tab[rand() % 256] << endl;
}
