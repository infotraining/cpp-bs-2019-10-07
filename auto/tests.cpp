#include <algorithm>
#include <deque>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <tuple>
#include <typeinfo>
#include <vector>

#include "catch.hpp"

using namespace std;

int counter = 0;

TEST_CASE("")
{
    int x = 42;

    int32_t i32 = 42;

    uint8_t small_number = 128;
}

TEST_CASE("auto")
{
    int x = 42;
    auto ax = 42; // int

    //auto ax_bad; c-error

    auto c = 'a';            // char
    auto pi = 3.14;          // double
    auto double_pi = pi * 2; // double
    auto e = 2.7f;           // float

    auto text = "text"; // const char*
    auto str = "text"s; // string - since C++14

    const vector<int> vec = { 1, 2, 3, 4, 5, 6 };
    auto it = vec.begin(); // vector<int>::const_iterator

    for (auto it = begin(vec); it < end(vec); it += 2)
    {
        cout << *it << " ";
    }
    cout << "\n";

    const auto n = 665; // const int
    auto& aref_x = x;   // int&
    auto& aref_n = n;   // const int&
}

namespace Cpp11
{
    auto div(const int nominator, const int denominator) -> tuple<int, int>
    {
        return make_tuple(nominator / denominator, nominator % denominator);
    }

    template <typename T>
    auto multiply(T a, T b) -> decltype(a * b)
    {
        return a * b;
    }

}

namespace Cpp14
{
    auto div(const int nominator, const int denominator)
    {
        return make_tuple(nominator / denominator, nominator % denominator);
    }

    template <typename T>
    auto multiply(T a, T b)
    {
        return a * b;
    }
}

auto foo(int a)
{
    return 42 * a;
}

TEST_CASE("auto type deduction for functions - Since C++14")
{
    int q, r;
    tie(q, r) = Cpp14::div(5, 2);

    decltype(foo(42)) x {}; //int
    x = foo(2);

    string str;

    cout << "Type of str: " << typeid(str).name() << endl;
    cout << "Type of tpl: " << typeid(tuple<int, string>).name() << endl;
}

void foo();

TEST_CASE("using foo from other cpp file")
{
    foo();
    foo();

    REQUIRE(counter == 2);
}
