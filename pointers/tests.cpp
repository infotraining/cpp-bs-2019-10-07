#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

#define ERROR_COMPILER

using namespace std;

int global_value = 42;

void foo(int* ptr)
{
    int x = 42;

    if (ptr)
        cout << "foo(int*: " << *ptr << ")\n";
    else
        cout << "foo(int*: nullptr)\n";
}

void foo(long l)
{
    cout << "foo(long:" << l << ")\n";
}

TEST_CASE("null pointers")
{
    SECTION("legacy - c++98")
    {
        int* ptr1 = NULL;
        int* ptr2 = 0;
    }

    int* ptr1 = nullptr;
    int* ptr2 {};

    REQUIRE(ptr1 == nullptr);
    REQUIRE(ptr2 == nullptr);

    int x = 10;
    foo(&x);
    foo(nullptr);
}

TEST_CASE("using pointers")
{
    int x = 42;

    int* ptr = &x;

    if (ptr != nullptr)
        REQUIRE(*ptr == 42);

    if (ptr)
        REQUIRE(*ptr == 42);

    cout << "ptr: " << ptr << endl;
    cout << "&ptr: " << &ptr << endl;

    SECTION("dereferencing nullptr is UB")
    {
        int* bad_ptr {};

        //REQUIRE(*bad_ptr == 13); // UB
    }
}

TEST_CASE("pointers with consts")
{
    int x = 42;
    const int y = 1024;

    // int* ptr = &y; // ERROR
    const int* ptr = &y;

    SECTION("pointer to const")
    {
        const int* ptr_to_const = &x;

        // *ptr_to_const = 665;  // c-error

        ptr_to_const = &y;
        REQUIRE(*ptr_to_const == 1024);
        // *ptr_to_const = 665;
    }

    SECTION("const pointer")
    {
        int* const ptr = &x;

        *ptr = 665;
        REQUIRE(x == 665);

        int z = 1024;

        // ptr = &z; // error
    }

    SECTION("const pointer to const")
    {
        const int* const const_ptr_to_const = &x;
        //*const_ptr_to_const = 665; // c-error
        //const_ptr_to_const = &y; // c-error
    }
}

int add(int a, int b)
{
    return a + b;
}

int multiply(int a, int b)
{
    return a * b;
}

int div(int a, int b, void (*on_div_error)(const string&))
{
    if (b == 0)
        on_div_error("Error#13");

    return a / b;
}

void div_error_console(const string& msg)
{
    cout << "Error! Second arg can't be zero. " << msg << endl;

    exit(1);
}

void log_to_file()
{
    //...
    exit(1);
}

void wrapped_log_to_file(const string&)
{
    log_to_file();
}

TEST_CASE("pointers to function")
{
    int (*ptr_fun)(int, int) = nullptr;
    ptr_fun = add;
    ptr_fun = &add; // the same as line above

    cout << "add(1, 2) = " << add(1, 2) << endl;
    cout << "ptr_fun(1, 2) = " << ptr_fun(1, 2) << endl;

    ptr_fun = multiply;
    cout << "ptr_fun(2, 3) = " << ptr_fun(2, 3) << endl;

    cout << "div(4, 0) = " << div(4, 0, wrapped_log_to_file) << endl;
    cout << "div(4, 0) = " << div(4, 0, [](const string&) { log_to_file(); }) << endl; // lambda expression
}

TEST_CASE("null terminated string")
{
    char text_a[] = { 'a', 'b', 'c', '\0' };
    text_a[0] = 'A';

    const char* text_b = "abc";

    REQUIRE(text_b[3] == '\0');
}
