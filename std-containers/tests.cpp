#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>

#include "catch.hpp"

using namespace std;

void print_array1(const int tab[], size_t size)
{
    for (auto it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << "\n";
}

void print_array2(const int* tab, size_t size)
{
    for (auto it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << "\n";
}

TEST_CASE("native arrays")
{
    int tab[10] = { 1, 2, 3 };

    for (const auto& item : tab)
        cout << item << " ";
    cout << "\n";
}

template <typename TParam, size_t N>
void print_array(const std::array<TParam, N>& arr)
{
    for (const auto& item : arr)
        cout << item << " ";
    cout << "\n";
}

template <typename TContainer>
void print(const TContainer& container)
{
    for (const auto& item : container)
        cout << item << " ";
    cout << "\n";
}

TEST_CASE("list")
{
    list<string> lst = { "1one", "11two", "2three" };

    lst.push_back("four");
    lst.push_front("zero");

    print(lst);

    lst.sort(std::greater<> {});

    print(lst);
}

template <typename T1, typename T2>
struct Pair {
    T1 first;
    T2 second;
};

TEST_CASE("std::array")
{
    std::array<int, 10> arr = { 1, 2, 3 };

    REQUIRE(arr.size() == 10);

    for (size_t i = 0; i < arr.size(); ++i)
        arr[i] = i;

    for (const auto& item : arr)
        cout << item << " ";
    cout << "\n";

    print_array1(arr.data(), arr.size());
    print_array(arr);

    std::array<int, 3> arr2;
    print_array(arr2);

    std::array<string, 5> words = { "one", "two", "three", "four", "five" };
    print_array(words);

    Pair<int, double> p1 { 1, 3.14 };
    std::pair<int, double> p2 { 1, 3.14 };

    std::array<Pair<int, double>, 3> pairs = { Pair<int, double> { 1, 3.14 }, p1, Pair<int, double> { 3, 1.13 } };
    //print_array(pairs);
}

template <typename TMap>
void print_map(const TMap& dict)
{
    for (const auto& kv_pair : dict)
        cout << kv_pair.first << " - " << kv_pair.second << "\n";
    cout << "\n";
}

TEST_CASE("maps - log insert, erase, find")
{
    map<int, string> dict = { { 2, "two" }, { 1, "one" }, { 3, "three" } };

    REQUIRE(dict[2] == "two");

    dict.insert(make_pair(0, "zero")); // always in C++
    dict.emplace(5, "five"); // since C++11

    for (const std::pair<const int, string>& kv_pair : dict)
        cout << kv_pair.first << " - " << kv_pair.second << "\n";
    cout << "\n";

    for (const auto& kv_pair : dict)
        cout << kv_pair.first << " - " << kv_pair.second << "\n";
    cout << "\n";

    // since C++17
    for (const auto& [key, value] : dict)
        cout << key << " - " << value << "\n";
    cout << "\n";

    auto pos = dict.find(3);

    if (pos != dict.end())
        cout << pos->first << " - " << pos->second << "\n";

    dict.erase(3);

    pos = dict.find(3);

    REQUIRE(pos == dict.end());

    cout << "\n";
    print_map(dict);
}

TEST_CASE("unordered_maps - log insert, erase, find")
{
    unordered_map<int, string> dict = { { 2, "two" }, { 1, "one" }, { 3, "three" } };

    REQUIRE(dict[2] == "two");

    dict.insert(make_pair(0, "zero")); // always in C++
    dict.emplace(5, "five"); // since C++11

    for (const std::pair<const int, string>& kv_pair : dict)
        cout << kv_pair.first << " - " << kv_pair.second << "\n";
    cout << "\n";

    for (const auto& kv_pair : dict)
        cout << kv_pair.first << " - " << kv_pair.second << "\n";
    cout << "\n";

    // since C++17
    for (const auto& [key, value] : dict)
        cout << key << " - " << value << "\n";
    cout << "\n";

    auto pos = dict.find(3);

    if (pos != dict.end())
        cout << pos->first << " - " << pos->second << "\n";

    dict.erase(3);

    pos = dict.find(3);

    REQUIRE(pos == dict.end());

    cout << "\n";
    print_map(dict);

    cout << "Number of buckets: " << dict.bucket_count() << " : Load factor: " << dict.load_factor() << "\n";
}
