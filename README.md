# Programming in C++

## Additonal information

### Doc

* https://infotraining.bitbucket.io/cpp-bs


### login and password for VM:

```
dev  /  pwd
```

### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

### proxy settings

We can add them to `.profile`

```
export http_proxy=http://aaa.bbb.ccc.ddd:port
export https_proxy=https://aaa.bbb.ccc.ddd:port
```

### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

## GIT

```
git clone https://bitbucket.org/infotraining/cpp-bs-2019-10-07.git
```

## Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)
* [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
* [C++ Reference](https://en.cppreference.com)
* [vcpkg](https://github.com/microsoft/vcpkg)


## Ankieta

* https://infotraining.pl/ankieta/cpp-basic-2019-10-07-kp
