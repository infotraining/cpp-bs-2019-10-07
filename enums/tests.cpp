#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum Coffee : uint8_t
{
    espresso = 1,
    latte, // 2
    americano = 48,
    lura // 49
};

enum class Tea
{
    earl_grey = 1,
    english_breakfast, // 2
    lura = 48,
    extra_lura // 49
};

TEST_CASE("enum class")
{
    Tea t = Tea::lura;

    t = static_cast<Tea>(2);

    switch (t)
    {
        case Tea::earl_grey:
            cout << "Wow!\n";
            break;
        case Tea::lura:
            cout << "Great!\n";
            break;
        default:
            cout << "Ok!\n";
            break;
    }

    int value = static_cast<int>(t);
}

TEST_CASE("enums")
{
    Coffee c1 = lura;

    c1 = static_cast<Coffee>(2);

    switch (c1)
    {
        case lura:
            cout << "Wow!\n";
            break;
        case americano:
            cout << "Great!\n";
            break;
        default:
            cout << "Ok!\n";
            break;
    }

    int value = c1;

    cout << "Coffee: " << c1 << endl;
}

TEST_CASE("string literals")
{
    const char* txt1 = "text1\n\ttext2";
    cout << txt1 << endl;

    const char* path = R"(C:\twoj katalog\nasz plik)";
    cout << path << endl;

    auto multiline = R"(line1
line2
line3
line4)";

    cout << multiline << "\n";
}

typedef int Integer; // old-school

using Double = double; // since C++11

template <typename Key>
using Dict = std::map<Key, std::string>;

TEST_CASE("using type aliases")
{
    Integer i = 42;

    Double d = 3.14;

    Dict<int> dict = { { 1, "one" }, { 2, "two" }, { 3, "three" } };
}
