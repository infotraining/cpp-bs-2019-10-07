#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "bankaccount.hpp"
#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Banking;

TEST_CASE("BankAccount")
{
    Banking::BankAccount ba1("Swobodny Jacob", 1'000'000);
    ba1.print();

    Banking::BankAccount ba2("Evil John");
    ba2.print();

    Banking::BankAccount ba3;
    ba3.print();

    REQUIRE(ba3.get_name() == "BankAccount#3");
}

TEST_CASE("interest rates")
{
    REQUIRE(BankAccount::get_interest_rate() == Approx(0.05));
    BankAccount ba("Adam Nowak", 1000);
    ba.set_interest_rate(0.1);
    ba.update_interest();
    REQUIRE(ba.get_balance() == Approx(1100));
}

TEST_CASE("default constructor")
{
    BankAccount ba1;
    BankAccount ba2;

    ba1.print();
    ba2.print();
}

TEST_CASE("vector of accounts")
{
    vector<BankAccount> accounts
        = {{"AA", 1000}, {"AB", 2343}, {"AA1", 100}};

    accounts[0].deposit(5.0);
    accounts.back().withdraw(99.99);

    cout << "\n-----\n";

    for (const auto& acc : accounts)
        acc.print();

    BankAccount::set_interest_rate(0.2);

    for (auto& acc : accounts)
        acc.update_interest();

    cout << "\n-----\n";

    for (const auto& acc : accounts)
        acc.print();

    BankAccount::set_interest_rate(0.05);
}
