#ifndef BANKACCOUNT_HPP
#define BANKACCOUNT_HPP

#include <cassert>
#include <string>

namespace Banking
{

    class BankAccount
    {
    public:
        BankAccount() = default; // default constructor

        BankAccount(std::string name, double balance = 0.0);

        void withdraw(double amount)
        {
            assert(check_amount(amount));
            balance -= amount;
        }

        void deposit(double amount);

        void update_interest()
        {
            balance += balance * interest_rate;
        }

        void print() const;

        double get_balance() const
        {
            return balance;
        }

        const std::string& get_name() const
        {
            return name;
        }

        int get_id() const;

        static double get_interest_rate();
        static void set_interest_rate(double value);

    private:
        const int id = gen_id();
        std::string name = "BankAccount#" + std::to_string(id);
        double balance = 0.0;
        static double interest_rate;
        static int counter;

        bool check_amount(double amount) const
        {
            return amount >= 0;
        }

        static int gen_id()
        {
            return ++counter;
        }
    };
}

#endif // BANKACCOUNT_HPP
