#include "bankaccount.hpp"
#include <iomanip>
#include <iostream>

using namespace Banking;
using namespace std;

double BankAccount::interest_rate = 0.05;
int BankAccount::counter = 0;

BankAccount::BankAccount(string name, double balance)
    : name(name)
    , balance(balance)
{
}

void BankAccount::deposit(double amount)
{
    assert(check_amount(amount));
    balance += amount;
}

void BankAccount::print() const
{
    cout << "BankAccount("
         << id << ", "
         << this->name << ", "
         << fixed << setprecision(2) << balance << ")\n";
}

int BankAccount::get_id() const
{
    return id;
}

double BankAccount::get_interest_rate()
{
    return interest_rate;
}

void BankAccount::set_interest_rate(double value)
{
    interest_rate = value;
}
