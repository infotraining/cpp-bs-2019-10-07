#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>

template <typename TContainer>
void print(const TContainer& container)
{
    for (const auto& item : container)
        std::cout << item << " ";

    std::cout << "\n";
}

#endif // UTILS_HPP
