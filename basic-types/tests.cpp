#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("basic types")
{
    SECTION("integral types")
    {
        int x = 42;
        unsigned int ux = 42u;

        long int lx = 345657l;
        unsigned long int ulx = 235235ul;

        long long int llx = 25345245ll;
        unsigned long long int ullx = 5245826378ull;
    }

    SECTION("real - floating types")
    {
        double dx = 3.14;
        float fx = 3.14f;
    }

    SECTION("boolean")
    {
        bool is_ok = true;
        is_ok = false;
    }
}

TEST_CASE("strings")
{
    SECTION("c-string")
    {
        const char* txt1 = "Ala";
        const char* txt2 = "Ola";

        //REQUIRE(txt1 == txt2);

        const char* alt_text = "Ala";
        REQUIRE(txt1 == alt_text);

        for (const char* it = txt1; *it != '\0'; ++it)
            cout << *it;
        cout << "\n";

        cout << txt1 << "\n";
    }

    SECTION("string")
    {
        string txt1 = "Ala";
        string txt2 = "Ola";

        txt1[0] = 'O';

        REQUIRE(txt1 == txt2);

        txt1 += " ma kota";

        cout << txt1 << endl;
    }
}

void print(const string& txt)
{
    cout << "Printing: " << txt << "\n";
}

void load_from_file(string& str)
{
    str = "Content";
}

TEST_CASE("passing string as args to function")
{
    string str;

    REQUIRE(str == string(""));

    load_from_file(str);

    REQUIRE(str == string("Content"));

    print(str);
}

void print(const vector<int>& vec)
{
    for (const auto& item : vec)
    {
        cout << item << " ";
    }

    cout << "\n";
}

vector<int> create_vec(size_t n)
{
    vector<int> vec(n);

    for (size_t i = 0; i < n; ++i)
        vec[i] = i;

    return vec;
}

TEST_CASE("vector - dynamic array")
{
    vector<int> vec = { 1, 2, 3, 4 };

    REQUIRE(vec.size() == 4);

    vec.push_back(5);

    REQUIRE(vec.size() == 5);

    print(vec);

    for (auto it = begin(vec); it != end(vec); ++it)
    {
        cout << *it << " ";
    }
    cout << "\n";

    vector<int> data = create_vec(15);

    print(data);
}
