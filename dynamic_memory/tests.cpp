#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    string name;

    Gadget(const string& n = "not-set") // constructor
        : name(n)
    {
        cout << "Gadget(" << name << ": " << this << ")\n";
    }

    ~Gadget() // destructor
    {
        cout << "~Gadget(" << name << ": " << this << ")\n";
    }

    void use()
    {
        cout << "Using " << name << ": " << this << "\n";
    }
};

TEST_CASE("dynamic memory")
{
    SECTION("local objects - stack")
    {
        Gadget g{"ipad"};
        g.use();

        Gadget gadgets[10];
        for (auto& g : gadgets)
            g.use();
    }

    SECTION("dynamic object - heap - object new")
    {
        for (int i = 0; i < 100; ++i)
        {
            Gadget* ptr_g = new Gadget("thompsonic" + to_string(i));
            ptr_g->use();

            delete ptr_g;
            ptr_g = nullptr;

            //ptr_g->use(); // BEWARE UB
        }
    }

    SECTION("dynamic arrays - heap - array new")
    {
        const size_t size = 10;
        Gadget* gadgets = new Gadget[size];

        //        for (Gadget* g = gadgets; g != gadgets + size; ++g)
        //            g->use();
        for (size_t i = 0; i < size; ++i)
            gadgets[i].use();

        delete[] gadgets;
    }

    SECTION("using malloc & placement")
    {
        void* raw_mem = malloc(sizeof(Gadget)); // memory allocation
        Gadget* ptr_g = new (raw_mem) Gadget("unitra"); // placement-new

        cout << raw_mem << " " << ptr_g << "\n";

        ptr_g->use();

        ptr_g->~Gadget(); // destruction of object
        free(raw_mem); // deallocation
    }

    SECTION("Modern C++ memory management - smart pointers")
    {
        SECTION("unique_ptr")
        {
            cout << "\n######################\n";

            unique_ptr<Gadget> ptr_g(new Gadget{"radmor"});
            ptr_g->use();
            (*ptr_g).use();

            unique_ptr<Gadget> other_g = std::move(ptr_g);
            REQUIRE(ptr_g == nullptr);
            other_g->use();

            vector<unique_ptr<Gadget>> gadgets;
            gadgets.push_back(std::move(other_g));
            REQUIRE(other_g == nullptr);
            gadgets[0]->use();
        }
    }
}

namespace Legacy
{
    Gadget* create_gadget(const string& name = "Gadget")
    {
        static int counter;
        return new Gadget(name + to_string(++counter));
    }

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(Gadget* g)
    {
        if (g)
            g->use();

        delete g;
    }
}

TEST_CASE("using legacy in modern way")
{
    Gadget* old_style_ptr = Legacy::create_gadget("legacy");
    std::unique_ptr<Gadget> ptr(old_style_ptr);
    old_style_ptr = nullptr;

    Legacy::use1(ptr.get());

    Legacy::use2(ptr.release());
}

TEST_CASE("mem leak")
{
    {
        Gadget* ptrg = Legacy::create_gadget("ipad");
        ptrg->use();

        Legacy::create_gadget("temp");
    } // mem leak

    {
        Legacy::use1(Legacy::create_gadget("ipad")); // mem leak
    }

    {
        Gadget g{"ipod"};
        Legacy::use1(&g);
        // Legacy::use2(&g); // SEGFAULT
    }
}

namespace Modern
{
    std::unique_ptr<Gadget> create_gadget(const string& name = "Gadget")
    {
        static int counter;

        //std::unique_ptr<Gadget> ptr(new Gadget(name + to_string(++counter)));
        auto ptr = std::make_unique<Gadget>(name + to_string(++counter)); // since C++14
        return ptr;
    }

    void use1(Gadget* g)
    {
        if (g)
            g->use();
    }

    void use2(std::unique_ptr<Gadget> g)
    {
        if (g)
            g->use();
    }
}

TEST_CASE("modern - ok")
{
    {
        std::unique_ptr<Gadget> ptrg = Modern::create_gadget("ipad");
        ptrg->use();

        Modern::create_gadget("temp");
    }

    {
        auto ptrg = Modern::create_gadget("ipad");
        Modern::use1(ptrg.get());
    }

    {
        Gadget g{"ipod"};
        Modern::use1(&g);
        Modern::use2(Modern::create_gadget("unitra"));
    }
}

TEST_CASE("shared ptrs")
{
    shared_ptr<Gadget> ptrg;

    {
        auto local = make_shared<Gadget>("mp3"); // rc == 1
        shared_ptr<Gadget> other = local; // rc == 2
        local->use();
        other->use();
        ptrg = local; // rc == 3
    } // rc == 1

    ptrg->use();

    auto backup = ptrg; // rc == 2

    ptrg.reset(); // rc == 1
} // rc == 0 -> delete mp3

TEST_CASE("delete for nullptr is safe")
{
    int* ptr = nullptr;

    if (true)
        ptr = new int(13);

    delete ptr;
}

void use_in_thread(shared_ptr<Gadget> g, int n)
{
    for (int i = 0; i < n; ++i)
    {
        cout << this_thread::get_id() << " - ";
        g->use();
        cout.flush();
        this_thread::sleep_for(100ms);
    }
}

TEST_CASE("shared_ptr & threads")
{
    auto g = make_shared<Gadget>("thd");

    thread thd1(use_in_thread, g, 10);
    thread thd2(use_in_thread, g, 20);

    g.reset();

    thd1.join();
    thd2.join();
}

class Person
{
    Gadget* gadget_;

public:
    Person(const string& name)
        : gadget_{new Gadget(name)}
    {
    }

    Person(const Person& source) // copy constructor
        : gadget_{new Gadget(source.gadget_->name)}
    {
    }

    Person& operator=(const Person& source) // copy assignment
    {
        if (this != &source)
        {
            delete gadget_;
            gadget_ = new Gadget(source.gadget_->name);
        }

        return *this;
    }

    Person(Person&& source) // move constructor - C++11
        : gadget_{source.gadget_}
    {
        source.gadget_ = nullptr;
    }

    Person& operator=(Person&& source) // move assignment - C++11
    {
        if (this != &source)
        {
            gadget_ = source.gadget_;
            source.gadget_ = nullptr;
        }

        return *this;
    }

    ~Person()
    {
        delete gadget_;
    }

    void have_fun()
    {
        gadget_->use();
    }
};

class UPerson
{
    std::unique_ptr<Gadget> gadget_;
    string name_;

public:
    UPerson(const string& name, const string& gname)
        : gadget_{new Gadget(gname)}
        , name_{name}
    {
    }

    UPerson(const UPerson&) = delete;
    UPerson& operator=(const UPerson&) = delete;
    UPerson(UPerson&&) = default;
    UPerson& operator=(UPerson&&) = default;

    void have_fun()
    {
        gadget_->use();
    }
};

TEST_CASE("Person with gadgets")
{
    cout << "\n--------------\n";

    Person p1("xbox");
    p1.have_fun();

    Person p2 = p1; // cc
    p2.have_fun();

    Person p3("pegasus");
    p3 = p2; // copy assignment
    p3 = std::move(p1); // move assignment

    UPerson up1("Jan", "playstation");
    UPerson up2 = std::move(up1);
    up1 = UPerson("Ewa", "nintendo");
    up2.have_fun();
}
