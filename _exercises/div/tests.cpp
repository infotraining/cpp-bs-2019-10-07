#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

#include "catch.hpp"

using namespace std;

void div(const int nominator, const int denominator, int& q, int& r)
{
    q = nominator / denominator;
    r = nominator % denominator;
}

// GCC only
void print(auto item)
{
    cout << item << "\n";
}

TEST_CASE("auto in gcc only")
{
    print(3.14);
    print("text");
    print(string("abc"));
    print(42);
    //print(make_tuple(1, 3.14));
}

namespace ModernCpp
{
    tuple<int, int> div(const int nominator, const int denominator)
    {
        tuple<int, int> result;
        get<0>(result) = nominator / denominator;
        get<1>(result) = nominator % denominator;
        return result;

        // return tuple<int, int>{nominator / denominator, nominator % denominator };
        // return { nominator / denominator, nominator % denominator };
        return make_tuple(nominator / denominator, nominator % denominator);
    }
}

TEST_CASE("div")
{
    int x = 5;
    int y = 2;

    int quotient, reminder;

    div(x, y, quotient, reminder);

    REQUIRE(quotient == 2);
    REQUIRE(reminder == 1);
}

TEST_CASE("div with tuple - Since C++11")
{
    int x = 5;
    int y = 2;

    tuple<int, int> result = ModernCpp::div(x, y);

    REQUIRE(get<0>(result) == 2);
    REQUIRE(get<1>(result) == 1);
}

TEST_CASE("tuple with tie - Since C++11")
{
    int x = 5;
    int y = 2;

    int quotient, reminder;
    tie(quotient, reminder) = ModernCpp::div(x, y);

    REQUIRE(quotient == 2);
    REQUIRE(reminder == 1);
}

TEST_CASE("Since C++17 - structured bindings")
{
    auto [quotient, reminder] = ModernCpp::div(5, 2);

    REQUIRE(quotient == 2);
    REQUIRE(reminder == 1);
}
