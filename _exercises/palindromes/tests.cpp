#include <algorithm>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Solution
{
    namespace Ver_1_0
    {
        bool is_palindrome(const string& text)
        {
            if (text.size() == 0)
                return true;

            for (unsigned int i = 0, j = text.size() - 1u; i <= j; ++i, --j)
            {
                if (text[i] != text[j])
                    return false;
            }

            return true;
        }
    }

    namespace Ver_2_0
    {
        bool is_palindrome(const string& text)
        {
            string txet(text.rbegin(), text.rend());
            //            for (char c : text)
            //                txet.insert(txet.begin(), c);

            return text == txet;
        }
    }

    namespace Ver_3_0
    {
        bool is_palindrome(const string& text)
        {
            string::const_iterator it1;
            string::const_reverse_iterator it2;
            for (it1 = text.begin(), it2 = text.rbegin(); it1 < it2.base(); ++it1, ++it2)
            {
                if (*it1 != *it2)
                    return false;
            }

            return true;
        }
    }

    inline namespace Ver_4_0
    {
        bool is_palindrome(const string& text)
        {
            return std::equal(text.begin(), text.begin() + text.size() / 2, text.rbegin());
        }
    }
}

TEST_CASE("palindromes")
{
    using namespace Solution;

    SECTION("is palindrome")
    {
        SECTION("empty string")
        {
            REQUIRE(is_palindrome(""));
        }

        SECTION("potop")
        {
            string str = "potop";
            REQUIRE(is_palindrome(str));
        }

        SECTION("abbccbbaa")
        {
            string str = "aabbccbbaa";
            REQUIRE(is_palindrome(str));
        }
    }

    SECTION("is not palindrome")
    {
        string str = "mars";

        REQUIRE_FALSE(is_palindrome(str));
    }
}

TEST_CASE("algorithms")
{
    vector<int> vec = { 1, 7, 2, 3, 8, 5 };

    REQUIRE(*(vec.begin() + 3) == 3);

    sort(begin(vec), end(vec));

    for (const auto& item : vec)
        cout << item << " ";
    cout << "\n";

    int tab[] = { 1, 7, 2, 3, 8, 5 };

    sort(begin(tab), end(tab));
}
