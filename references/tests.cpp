#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("references")
{
    int x = 10;

    int& ref_x = x;

    ref_x = 42;
    REQUIRE(x == 42);

    x = 665;
    REQUIRE(ref_x == 665);

    REQUIRE(&ref_x == &x); // address of ref is the same as address of original obj

    const int& cref_x = x;
}

void clear(int* x)
{
    if (x)
        *x = 0;
}

void clear(int& a)
{
    a = 0;
}

void print(const string& str)
{
    cout << str << "\n";
}

TEST_CASE("passing args to function")
{
    int x = 13;
    clear(&x);
    REQUIRE(x == 0);

    x = 42;
    clear(x);
    REQUIRE(x == 0);

    string text = "text";
    print(text);

    void (*ptr_fun)(const string&) = print;
    void (&ref_fun)(const string&) = print;
}
