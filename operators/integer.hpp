#ifndef INTEGER_HPP
#define INTEGER_HPP

#include <iostream>

namespace Values
{
    struct Integer
    {
        Integer(int v)
            : value_(v)
        {
        }

        Integer operator-() const // -a
        {
            return Integer{-value_};
        }

        int value() const
        {
            return value_;
        }

    private:
        int value_;

        friend Integer operator-(const Integer& left, const Integer& right);
        friend bool operator==(const Integer& left, const Integer& right);
        friend std::ostream& operator<<(std::ostream& out, const Integer& i);
    };

    inline Integer operator+(const Integer& left, const Integer& right)
    {
        return Integer{left.value() + right.value()};
    }

    inline Integer operator-(const Integer& left, const Integer& right) // a - b
    {
        return Integer{left.value_ - right.value_};
    }

    inline bool operator==(const Integer& left, const Integer& right)
    {
        return left.value_ == right.value_;
    }

    inline bool operator!=(const Integer& left, const Integer& right)
    {
        return !(left == right);
    }

    std::ostream& operator<<(std::ostream& out, const Integer& i);
}

#endif // INTEGER_HPP
