#include "integer.hpp"

std::ostream& Values::operator<<(std::ostream& out, const Values::Integer& i)
{
    out << i.value_;

    return out;
}
