#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "integer.hpp"

using namespace std;
using namespace Values;

TEST_CASE("operators")
{

    Integer i1{1};
    Integer i2(4);
    Integer i3{665};
    // Integer i4; // c-error

    Integer r1 = -i1;
    REQUIRE(r1.value() == -1);

    Integer r2 = i1 + i2; // operator+(i1, i2);
    REQUIRE(r2.value() == 5);

    Integer r3 = i1 - i2; // i1.operator-(i2)
    REQUIRE(((i1 - i2) == (i1 + -i2)));

    Integer r4a = 4 + i1;
    Integer r4b = i1 + 4;
    Integer r5 = 4 - i1;

    REQUIRE(Integer{1} == 1);
    //REQUIRE(1 == Integer{1});

    cout << "i1 = " << i1 << endl;
}
